#!/usr/bin/env node

var
	Assword = require("./"),
	
	argv = process.argv.slice(2);

if (argv.indexOf("--help") + 1 || argv.length < 1) {
	console.error("Usage: assworder [length] {length type (String/Byte)}");
	
	return;
}

console.log(String(new Assword(argv[0], argv[1])));
